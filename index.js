// [SECTION] Document Object Model
// it allows us to access or modify the properties of an element in a webpage.
// it is a standard on how to get, change, add or delete HTML elements.
/*
	syntax: 
		document.querySelector("htmlElement");

		 - The querySelector function takes a string input that is formatted like a CSS Selector when applying styles.
*/

// document.getElementById("txt-first-name");
// document.getElementByClassName("txt-last-name");
// however, using these functions requires us to identify beforehanbd we get the element. with querySelector, we can be flexible in how to retrieve elements

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event Listeners
// Whenever a user interacts with a webpage, this action is considered as an event. (example:)

// addEventListener takes two arguments:
	// a string that identify an event
	// a function that the listener will execute once "specified event" is triggered.
/*
txtFirstName.addEventListener("keyup", ()=>{
	// .innerHTML property sets/returns the HTML content of an element (div, spans, etc.)
	// .value property sets or returns the value of an attribute (form controls)

	spanFullName.innerHTML = `${txtFirstName.value}`;
})
*/
/*
txtFirstName.addEventListener("keyup", (event)=>{
	console.log(event.target);
	console.log(event.target.value);
})

*/

const fullName = () =>{
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

function changeColor(event)
{
	var color = event.value;
	document.getElementById("span-full-name").style.color=color;
}
